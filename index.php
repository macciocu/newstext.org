<?php
date_default_timezone_set('UTC'); /* 'America/Los_Angeles' */

$link = 'index.php';
if (isset($_GET['link']))
    $link = $_GET['link'];

$pw = '';
if (isset($_GET['pw'])) 
    $pw = $_GET['pw'];

$admin = ($pw == 'macciocu') ? true : false;

function getTitle($link) {
    $title = '';
    
    if ($link == 'index.php') {
        $title = 'Latest breaking news headlines - newstext.org';
    } else {
        $title = str_replace('_', ' ', $link);
        $title = str_replace('-', ' ', $title);
        $title = $title.' - newstext.org, latest breaking news headlines';
    }
    return ($title);
}

function writeMenuLinks($link, $name, $img) {
    $r = '';
    $j = count($link);
    
    for ($i=0; $i<$j; $i++) {
        $r .= 
            '<a href="?link='.$link[$i].'" title="'.$name[$i].'">'
                .'<img src="./img/icons_white_16x16/'.$img[$i].'" '
                    .'alt="'.$name[$i].'" height="16" width="16"/>'.$name[$i]
            .'</a><br/>';
    }
    return $r;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo (getTitle($link)); ?></title>
<meta charset="UTF-8"/>
<meta name="description" content="The latest breaking news headlines from around the world, tracking thousands of sources in one place."/>
<meta name="keywords" content="headlines, breaking,  latest, news, live, current events, text"/>

<base target="_self"/>
    
<link rel=Stylesheet type="text/css" media=screen href="./css/layout.css">
<link rel=Stylesheet type="text/css" media=screen href="./css/popup.css">
<link rel=Stylesheet type="text/css" media=screen href="./css/cssmap-continents.css">

<!-- http://encosia.com/3-reasons-why-you-should-let-google-host-jquery-for-you/ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>

<script type="text/javascript">
if (!window.jQuery) {
    document.write('<script src="./js/jquery/jquery.js"><\/script>');
}
</script>

<script type="text/javascript" charset="utf-8" src="./js/jquery.pinnedFooter.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/jquery.pinnedHeader.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/jquery.tabs.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/jquery.cssmap.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/fw.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/popup.js"></script>

<?php include("./php/rssTracker.inc.php"); ?>

<script type="text/javascript" charset="utf-8" src="./js/startup.js"></script>
<script type="text/javascript" charset="utf-8" src="./js/navmenu.js"></script>
<script type="text/javascript">
/*<![CDATA[*/

/** header positioning **/

/*
// Default positioning: "Static" or constant pinned footer
$("#id_header").pinHeader();
// Footer pinned if content less than window height.
// $("#footer").pinFooter("relative");

// launching code on window resizing
$(window).resize(function() {
    $("#id_header").pinHeader();
});

// launching code on document ready
$(document).ready(function() {
    $("#id_header").pinHeader();
});
*/

/** footer positioning **/

// Default positioning: "Static" or constant pinned footer
$("#container_footer").pinFooter();
// Footer pinned if content less than window height.
// $("#footer").pinFooter("relative");

// launching code on window resizing
$(window).resize(function() {
    $("#container_footer").pinFooter();
});

// launching code on document ready
$(document).ready(function() {
    $("#container_footer").pinFooter();
});

/** startup **/

var rssTracker;

$(document).ready(function() {
    $('#economy-map_continents').cssMap({ 'size':320, 'visibleList':false, 'cities':true });
    
    <?php if ($link != "index.php") { ?>
        $("#tabsholder").tytabs({ tabinit:"1", /* active tab */ fadespeed:"fast" });
        $("#content1, #content2").html('<img src="./img/loader.gif" width="128" height="15"/>');
    
        if (("<?php echo($link); ?>").length > 2) {
            var htm = "";
            var spl = ("<?php echo($link); ?>").split("-");
    
            for (var i=0; i<spl.length; i++) {
                htm += spl[i].replace(/_/g, " ");
                if (i < spl.length - 1) { htm += " | "; }
            }
    
            $("#id_category").html("<h1>" + htm + "</h1>");
            rssTracker = new startup("<?php echo($link); ?>");
        }
    <?php } else { ?>
        navmenu.en("id_menu_0", "menu");
    <?php } ?>
});    
/*]]>*/ 
</script>
</head>
<body>
<div id="popup-page-disable"></div>
<div id="popup"></div>

<div id="container_header">
    <div id="container_header_top">
        <div id="menu" onclick="navmenu.en('id_menu_0', this.id);">
            <img src="img/icons_white_16x16/br_down.png" height="16" height="16" alt="menu"/>
            Menu
        </div>
        <div id="logo_symbol">
            <img src="img/icons_white_32x32/rss.png" height="24" width="24" alt="logo"/>
        </div>
        <div id="logo_text">
            NewsText.org
        </div>
    </div>
    <div id="container_header_bottom">
        <div class="l" id="id_category">
            <!-- generated content -->
        </div>
        <div class="r2">
            <a href="#" title="search" onclick="rss.search(rssTracker.search('#id_search_txt'))">
                <img src="img/search.png" height="16" width="16" alt="search"/>
            </a>
        </div>
        <div class="r1">
            <input type="text" id="id_search_txt" onkeydown="if (event.keyCode == 13) rssTracker.search('#id_search_txt');"/>
        </div>
    </div>
</div>

<div id="id_submenu_transparent_bg" class="hidden">
    <!-- menu transparent overlay (handled by navmenu.js) -->
</div>

<div id="id_menu_0" class="container_menu">
    <table class="submenu">
        <tr>
            <td colspan="4" style="text-align:right">
                <h1>Breaking News Headlines</h1>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h2>News</h2>
            </td>
            <td colspan="2" style="padding-left:40px;">
                <h2>Finance</h2>
            </td>
        </tr>
        <tr>
            <td class="td1">
                <h3>International</h3><br/>
                <?php
                echo (writeMenuLinks(array(
                    'News-International-Headlines',
                    'News-International-Business_and_Economy',
                    'News-International-Politics',
                    'News-International-Health',
                    'News-Science_and_Technology',
                    'News-Space_Research'
                    ),array(
                    'Headlines',
                    'Business&nbsp;&amp;&nbsp;Economy',
                    'Politics',
                    'Health',
                    'Science&nbsp;&amp;&nbsp;Technology',
                    'Space&nbsp;Research'
                    ),array(
                    "globe_3.png",
                    "chart_line_2.png",
                    "layers_1.png",
                    "heart_empty.png",
                    "cog.png",
                    "sat_dish.png")));
                ?>
            </td>
            <td class="td2">
                <h3>Regional</h3><br/>
                <div id="economy-map_continents">
                    <ul class="continents">
                        <li class="c1">
                            <a href="?link=News-Regional-Africa">Africa</a>
                        </li>
                        <li class="c2">
                            <a href="?link=News-Regional-Middle_East_and_Asia">Middle East &amp; Asia</a>
                        </li>
                        <li class="c3">
                            <a href="?link=News-Regional-Australia">Australia</a>
                        </li>
                        <li class="c4">
                            <a href="?link=News-Regional-Europe">Europe</a>
                        </li>
                        <li class="c5">
                            <a href="?link=News-Regional-North_America">North America</a>
                        </li>
                        <li class="c6">
                            <a href="?link=News-Regional-South_America">South America</a>
                        </li>
                    </ul>
                </div>
            </td>
            <td class="td3">
                <h3>Market Data</h3><br/>
                <?php
                echo (writeMenuLinks(array(
                    'Finance-Market_Data-Headlines',
                    'Finance-Market_Data-Currencies',
                    'Finance-Market_Data-Index_Based_Trading',
                    'Finance-Market_Data-Mergers_and_Acquisitions',
                    'Finance-Market_Data-Stock_Spotlight',
                    'Finance-Market_Data-Upgrades_and_Downgrades',
                    'Finance-Market_Data-Sector_Updates'
                    ),array(
                    'Headlines',
                    'Currencies',
                    'Index&nbsp;based&nbsp;trading',
                    'Mergers&nbsp;and&nbsp;Acquisitions',
                    'Stock&nbsp;Spotlight',
                    'Upgrades&nbsp;and&nbsp;Downgrades',
                    'Sector&nbsp;Updates'
                    ),array(
                    "fire.png",
                    "cur_yen.png",
                    "chart_bar.png",
                    "shop_cart.png",
                    "star.png",
                    "cursor_H_split.png",
                    "chart_pie.png")));
                ?>
            </td>
            <td>
            <h3>Investing Analysis &amp; Insights</h3><br/>
                <?php
                echo (writeMenuLinks(array(
                    'Finance-Investing_Analysis_and_Insights-Blogs',
                    'Finance-Investing_Analysis_and_Insights-Articles'
                    ),array(
                    'Blogs',
                    'Articles',
                    ),array(
                    "user.png",
                    "preso.png")));
                ?>
            </td>
        </tr>
    </table>
    <div id="menu_footer">
        <!-- reserved -->
    </div>
</div>

<?php if ($link != "index.php") { ?>
    <div id="container_main">
        <div id="tabsholder">
            <div id="container_tabs">
                <ul class="tabs">
                    <li id="tab1">
                        Latest Entries
                    </li>
                    <li id="tab2">
                        Headlines
                    </li>
                    <li id="tab3" style="display:none">
                        Search Results
                    </li>
                </ul>
            </div>
            <div class="contents marginbot">
                <div id="content1" class="tabscontent">
                    <!-- generated content-->
                </div>
                <div id="content2" class="tabscontent">
                    <!-- generated content -->
                </div>
                <div id="content3" class="tabscontent">
                    <!-- generated content-->
                </div>
            </div>
        </div>
        <div id="content_rssinstances">
            <!-- generated content-->
        </div>
    </div>
<?php } ?>

<div id="container_footer">
    <div id="top">
        <!-- fading white bottom (css) -->
    </div>
    <div id="bottom">
        <label id="copyright">
            Copyright &copy; 2013 FinanceBuzz.com, All rights reserved.
        </label>
    </div>
</div>
</body>
</html>