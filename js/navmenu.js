/*
*******************************************************************************
** navmenu.js                                                author: GMacciocu
** (c) Copyright 2011-2013 - All Rights Reserved
*******************************************************************************
*/

var navmenu = {
    ITEMS: 3,
    OUTERID: "id_submenu_transparent_bg",
    OUTERID_OULINE: "id_submenu_transparent_bg",
    TRANSITION_INTERVAL: 20,
    OPACITY_DARK: 0.95,    
    
    st: undefined,
    busy: false,
    contentIDhistory: "",
    navmenuIDhistory: "",
    
    // PRIVATE METHODS
     
    fade: {
        o_busy : false,
        i_busy : false,
    
        /**
         * navmenu.fade.In
         */
        In: function(opacity) {
            if (opacity < navmenu.OPACITY_DARK) {
                navmenu.i_busy = true;
                // fade-in animation
                opacity += 0.1;
                $("#"+navmenu.OUTERID)[0].style.opacity = opacity;
                setTimeout(function() { 
                    navmenu.fade.In(opacity);
                }, navmenu.TRANSITION_INTERVAL);
            } else {
                navmenu.i_busy = false;
            }
        },
        
        /**
         * navmenu.fade.Out
         */
        Out: function(opacity) {
            if (opacity > 0) {
                navmenu.o_busy = true;
                // fade-out animation
                opacity -= 0.1;
                $("#"+navmenu.OUTERID)[0].style.opacity = opacity;
                setTimeout(function() {
                    navmenu.fade.Out(opacity);
                }, navmenu.TRANSITION_INTERVAL);
            } else {
                navmenu.o_busy = false;
                $("#"+navmenu.OUTERID_OULINE)[0].style.height = "0%";
                fw.ui.single.en(navmenu.OUTERID, false);
            }
        }
    },
    
    /**
     * navmenu.finish
     */
    finish: function(contentID, navMenuID) {
        if (navmenu.fade.i_busy || navmenu.fade.o_busy) {
            setTimeout(function() {
                navmenu.finish(contentID, navMenuID); 
            }, navmenu.TRANSITION_INTERVAL);
        } else {
            navmenu.busy = false;
            navmenu.navmenuIDhistory = navMenuID
            navmenu.contentIDhistory = contentID;
        }
    },
    
    // PUBLIC METHODS
    
    /**
     * navmenu.en
     */
    en: function(contentID, navMenuID) {
        if (navmenu.busy) {
            return;
        }
        
        navmenu.busy = true;
        
        var ST = { FADEIN: 0, FADEOUT: 1, FADEOUT2FADEIN: 2 };
        var fade_in = false;
        var fade_out = false;
        var release = (navmenu.navmenuIDhistory == navMenuID) ? true : false;
                
        if (navmenu.st == "undefined" || navmenu.st == null) {
            // status: startup, fade menu in
            navmenu.st = ST.FADEIN;
        
        } else if ((navmenu.st === ST.FADEIN || 
                    navmenu.st === ST.FADEOUT2FADEIN) && 
                   (release && navmenu.contentIDhistory === contentID)) {
            // status: fade menu out
            navmenu.st = ST.FADEOUT;
        
        } else if (navmenu.st === ST.FADEIN || 
                   navmenu.st === ST.FADEOUT2FADEIN) {
            // status: fade menu a out to fade menu b in (only in case of multiple menus)
            navmenu.st = ST.FADEOUT2FADEIN;
        
        } else if (navmenu.st === ST.FADEOUT) {
            // status: fade menu in
            navmenu.st = ST.FADEIN;
        }
        
        switch (navmenu.st) {
            case ST.FADEIN:
                fade_in = true;
                fade_out = false;
                break;
            case ST.FADEOUT:
                fade_in = false;
                fade_out = true;
                break;
            case ST.FADEOUT2FADEIN:
                fade_in = true;
                fade_out = true;
                break;
        }
        
        if (navmenu.contentIDhistory != "") {
            fw.ui.single.en(navmenu.contentIDhistory, false);
        }
        
        if (navmenu.contentIDhistory != "" && fade_out && !fade_in) {
            navmenu.fade.Out(navmenu.OPACITY_DARK);
        }
        
        if (fade_in) {
            fw.ui.single.en(contentID, true);
            fw.ui.single.en(navmenu.OUTERID, true);
            if (navmenu.st != ST.FADEOUT2FADEIN) { navmenu.fade.In(0); }
            $("#"+navmenu.OUTERID_OULINE)[0].style.height = "100%";
        }
        
        navmenu.finish(contentID, navMenuID);
    }
};