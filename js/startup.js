/*
*******************************************************************************
** startup.js                                                author: GMacciocu
** (c) Copyright 2011-2013 - All Rights Reserved
*******************************************************************************
*/

function startup(sw) {
    this.rssurl = new Array();
    this.linkurl = new Array();
    this.feedname = new Array();

    var section = [
        ['News-International-Headlines',
         this.news_international_headlines],
        ['News-International-Business_and_Economy',
         this.news_international_business_and_economy],
        ['News-International-Politics',
         this.news_international_politics],
        ['News-International-Health',
         this.news_international_health],
        ['News-Science_and_Technology',
         this.news_science_and_technology],
        ['News-Space_Research',
         this.news_space_research],
        ['News-Regional-Africa',
         this.news_regional_africa],
        ['News-Regional-Middle_East_and_Asia',
         this.news_regional_middle_east_and_asia],
        ['News-Regional-Australia',
         this.news_regional_australia],
        ['News-Regional-Europe',
         this.news_regional_europe],
        ['News-Regional-North_America',
         this.news_regional_north_america],
        ['News-Regional-South_America',
         this.news_regional_south_america],
        ['Finance-Market_Data-Headlines',
         this.finance_market_data_headlines],
        ['Finance-Market_Data-Currencies',
         this.finance_market_data_currencies],
        ['Finance-Market_Data-Index_Based_Trading',
         this.finance_market_data_index_based_trading],
        ['Finance-Market_Data-Mergers_and_Acquisitions',
         this.finance_market_data_mergers_and_acquisitions],
        ['Finance-Market_Data-Stock_Spotlight',
         this.finance_market_data_stock_spotlight],
        ['Finance-Market_Data-Upgrades_and_Downgrades',
         this.finance_market_data_upgrades_and_downgrades],
        ['Finance-Market_Data-Sector_Updates',
         this.finance_market_data_sector_updates],
        ['Finance-Investing_Analysis_and_Insights-Blogs',
         this.finance_investing_analysis_and_insights_blogs],
        ['Finance-Investing_Analysis_and_Insights-Articles',
         this.finance_investing_analysis_and_insights_articles]];
    
    for (var i=0; i<=section.length; i++) {
        if (sw == section[i][0]) {
            var fp = section[i][1];
            fp(this);
            break;
        }
    }
    
    return (new RssTracker(this.feedname, this.rssurl, this.linkurl));
}

/*!
 * @brief
 * init this.rssurl, this.linkurl, this.feedname and create RssTracker object
 * @param
 * x[0][] = rssurl array
 * x[0][] = linkurl array
 * x[0][] = feedname array
 */
startup.prototype.init = function(x) {
    for (var i=0; i<x[0].length; i++) {
        this.rssurl[i] = x[0][i];
        this.linkurl[i] = x[1][i];
        this.feedname[i] = x[2][i];
    }
};

// News-International

startup.prototype.news_international_headlines = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_world.rss",
        "http://feeds.reuters.com/reuters/topNews?format=xml",
        "http://feeds.cbsnews.com/CBSNewsMain?format=xml",
        "http://feeds.bbci.co.uk/news/rss.xml",
        "http://rss.nytimes.com/services/xml/rss/nyt/GlobalHome.xml",
        "http://news.sky.com/feeds/rss/home.xml",
        "http://feeds.washingtonpost.com/rss/world"
        ],[
        "http://edition.cnn.com/WORLD/",
        "http://reuters.com/",
        "http://www.cbsnews.com/",
        "http://www.bbc.co.uk/news/",
        "http://www.nytimes.com/",
        "http://news.sky.com/",
        "http://www.washingtonpost.com/world"
        ],[
        "CNN - World",
        "Reuters - Top News",
        "CBS News- Breaking Headlines",
        "BBC - Home Page",
        "New York Times - Home Page (Global Edition)",
        "Sky News - Home Page",
        "The Washinton Post - World News"]]);
};

startup.prototype.news_international_business_and_economy = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_business.rss",
        "http://feeds.reuters.com/news/economy?format=xml",
        "http://feeds.cbsnews.com/CBSNewsBusiness?format=xml",
        "http://feeds.bbci.co.uk/news/business/economy/rss.xml",
        "http://news.sky.com/feeds/rss/business.xml",
        "http://feeds.washingtonpost.com/rss/business"
        ],[
        "http://edition.cnn.com/business/",
        "http://www.reuters.com/finance/economy",
        "http://www.cbsnews.com/moneywatch/",
        "http://www.bbc.co.uk/news/business/economy/",
        "http://news.sky.com/business",
        "http://www.washingtonpost.com/business"
        ],[
        "CNN - Business edition",
        "Reuters - Economy",
        "CBS Moneywatch - Business News",
        "BBC - Business &amp; Economy",
        "Sky - Business News",
        "The Washington Post - Business News"]]);
};

startup.prototype.news_international_politics = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/cnn_allpolitics.rss",
        "http://feeds.reuters.com/Reuters/PoliticsNews?format=xml",
        "http://feeds.cbsnews.com/CBSNewsPolitics?format=xml",
        "http://news.sky.com/feeds/rss/politics.xml",
        "http://feeds.washingtonpost.com/rss/politics"
        ],[
        "http://edition.cnn.com/POLITICS/",
        "http://blogs.reuters.com/breakingviews/category/politics/",
        "http://www.cbsnews.com/politics/",
        "http://news.sky.com/politics",
        "http://www.washingtonpost.com/politics"
        ],[
        "CNN - Politics",
        "Reuters - Politics",
        "CBS News - Politics",
        "Sky - Politics",
        "The Washington Post - Politics"]]);
};

startup.prototype.news_international_health = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/cnn_health.rss",
        "http://feeds.reuters.com/reuters/healthNews?format=xml",
        "http://feeds.cbsnews.com/CBSNewsHealth?format=xml",
        "http://www.nlm.nih.gov/medlineplus/feeds/news_en.xml",
        "http://rss.nytimes.com/services/xml/rss/nyt/Research.xml",
        "http://feeds.feedburner.com/mnt/healthnews?format=xml",
        "http://feeds.feedburner.com/NhsChoicesBehindTheHeadlines?format=xml"
        ],[
        "http://edition.cnn.com/health/",
        "http://www.reuters.com/news/health",
        "http://www.cbsnews.com/health/",
        "http://www.nlm.nih.gov/medlineplus/",
        "http://www.nytimes.com/pages/health/index.html",
        "http://http://www.medicalnewstoday.com",
        "http://www.nhs.uk/"
        ],[
        "CNN - Health",
        "Reuters - Health",
        "CBS News- Health",
        "U.S. National Library of Medicine",
        "New York Times - Health",
        "medicalnewstoday.com - headlines",
        "NHS - Health Science Research"]]);
};

startup.prototype.news_science_and_technology = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_technology.rss",
        "http://feeds.reuters.com/reuters/technologyNews",
        "http://news.sky.com/feeds/rss/technology.xml",
        "http://feeds.sciencedaily.com/sciencedaily/top_news/top_technology",
        "http://www.wired.com/wiredscience/feed/",
        "http://www.extremetech.com/feed",
        "http://feeds.feedburner.com/techcrunch/startups?format=xml",
        "http://feeds.feedburner.com/techcrunch/fundings-exits?format=xml",
        "http://feeds.sciencedaily.com/sciencedaily/computers_math/mathematics",
        "http://phys.org/rss-feed/science-news/mathematics/",
        "http://feeds.feedburner.com/PhysicsWorldNews?format=xml",
        "http://www.physnews.com/feed.xml",
        "http://feeds.feedburner.com/physicstoday/pt1"
        ],[
        "http://www.cnn.com/tech/",
        "http://www.reuters.com/news/technology",
        "http://news.sky.com/technology",
        "http://www.wired.com/wiredscience/",
        "http://www.sciencedaily.com",
        "http://www.extremetech.com/",
        "http://techcrunch.com/startups/",
        "http://techcrunch.com/fundings-exits/",
        "http://www.sciencedaily.com/news/computers_math/mathematics/",
        "http://www.physorg.com/science-news/mathematics/",
        "http://physicsworld.com/",
        "http://www.physnews.com",
        "http://www.physicstoday.org"
        ],[
        "CNN - Technology",
        "Reuters - Technology",
        "Sky -  Latest Technology and Gadget News",
        "wired.com - Science",
        "sciencedaily.com",
        "Techcrunch - Startups",
        "Techcrunch - Funding &amp; Exits",
        "sciencedaily.com - mathematics news",
        "PHYSorg.com - Mathematics News",
        "physicsworld.com",
        "physnews.com",
        "physicstoday.org"]]);
};

startup.prototype.news_space_research = function(t) {
    t.init([[
        "http://www.nasa.gov/rss/breaking_news.rss",
        "http://www.nasa.gov/mission_pages/kepler/news/kepler-newsandfeatures-RSS.rss",
        "http://www.nasa.gov/rss/solar_system.rss",
        "http://www.nasa.gov/rss/chandra_images.rss",
        "http://www.nasa.gov/rss/aeronautics.rss",
        "http://www.nasa.gov/rss/earth.rss",
        "http://www.nasa.gov/rss/universe.rss",
        "http://www.nasa.gov/rss/shuttle_station.rss",
        "http://www.nasa.gov/mission_pages/SOFIA/sofia-newsandfeatures-RSS.rss"
        ],[
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov",
        "http://www.nasa.gov"
        ],[
        "nasa - breaking news",
        "nasa - kepler mission",
        "nasa - solar system exploration",
        "nasa - Chandra X-ray Observatory Images",
        "nasa - aeronautics news",
        "nasa - earth news",
        "nasa - universe news",
        "nasa - Space Shuttle and International Space Station News",
        "nasa - SOFIA, NASA’s Stratospheric Observatory for Infrared Astronomy"]]);
};

// News-Regional

startup.prototype.news_regional_africa = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_africa.rss",
        "http://feeds.washingtonpost.com/rss/world/africa",
        "http://allafrica.com/tools/headlines/rdf/business/headlines.rdf",
        "http://www.southafrica.info/news/businessfeed.xml",
        "http://www.sharenet.co.za/feeds/sharenet_sens.xml",
        ],[
        "http://edition.cnn.com/africa/",
        "http://www.washingtonpost.com/world/africa",
        "http://allafrica.com",
        "http://www.southafrica.info",
        "http://www.sharenet.co.za/",
        ],[
        "CNN - Africa",
        "The Washington Post - Africa",
        "allafrica.com",
        "southafrica.info",
        "Sharenet - JSE Stock Exchange News Headlines"]]);
};

startup.prototype.news_regional_australia = function(t) {
    t.init([[
        "http://www.abc.net.au/news/feed/46182/rss.xml",
        "http://feeds.feedburner.com/newscomauthenationndm?format=xml",
        "http://feeds.feedburner.com/TheAustralianTheNationNews?format=xml",
        "http://sbs.feedsportal.com/c/34692/f/637524/index.rss",
        "http://feeds.news.com.au/heraldsun/rss/heraldsun_news_morenews_2794.xml"
        ],[
        "http://www.abc.net.au/news/act/",
        "http://www.news.com.au/national-news",
        "http://www.theaustralian.com.au/news/nation",
        "http://www.sbs.com.au/news/national/",
        "http://www.heraldsun.com.au/news/national"
        ],[
        "ABC News - Australia National News",
        "News.com.au - Australia National News",
        "The Australian - Australia National News",
        "SBS - Australia National News",
        "Herald Sun - Australia National News"]]);
};

startup.prototype.news_regional_europe = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_europe.rss",
        "http://www.cnbc.com/id/19811193/device/rss/rss.html",
        "http://www.ft.com/rss/home/europe",
        "http://www.eubusiness.com/rss",
        "http://www.eubusiness.com/topics/trade/aggregator/RSS",
        "http://www.ft.com/rss/markets/europe",
        "http://www.cnbc.com/id/19811193/device/rss/rss.xml",
        "http://news.sky.com/feeds/rss/uk.xml",
        "http://feeds.washingtonpost.com/rss/world/europe"
        ],[
        "http://edition.cnn.com/europe/",
        "http://www.cnbc.com/id/19794221/",
        "http://www.ft.com/intl/world/europe",
        "http://EUbusiness.com",
        "http://www.eubusiness.com",
        "http://www.ft.com/intl/markets/europe",
        "http://www.cnbc.com/id/19794221/site/14081545/",
        "http://news.sky.com/uk",
        "http://www.washingtonpost.com/world/europe"
        ],[
        "CNN - Europe",
        "CNBC - Europe",
        "Financial Times - Europe",
        "EUbusiness.com - E.U. Business News",
        "EUbusiness.com - E.U. external trade news",
        "Finance Times - European Markets",
        "CNBC - Europe Top News and Analysis",
        "Sky - UK News",
        "The Washington Post - Europe"]]);
};

startup.prototype.news_regional_middle_east_and_asia = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_asia.rss",
        "http://rss.cnn.com/rss/edition_meast.rss",
        "http://feeds.bbci.co.uk/news/world/middle_east/rss.xml",
        "http://www.economist.com/topics/chinese-economy/index.xml",
        "http://asianewsnetwork.feedsportal.com/c/33359/f/566603/index.rss",
        "http://www.apec.org/RssFeed/RSS.aspx",
        "http://www.chinapost.com.tw/rss/business.xml",
        "http://www.zawya.com/rssfeeds/economy_politics.xml",
        "http://feeds.washingtonpost.com/rss/world/asia-pacific",
        "http://www.washingtonpost.com/world/middle-east"
        ],[
        "http://edition.cnn.com/asia/",
        "http://edition.cnn.com/middleeast",
        "http://www.bbc.co.uk/news/world/middle_east/",
        "http://www.economist.com/topics/chinese-economy",
        "http://www.asianewsnet.net/home/index.php",
        "http://www.apec.org/",
        "http://www.chinapost.com.tw/business/",
        "http://www.zawya.com/money/",
        "http://www.washingtonpost.com/world/asia-pacific",
        "http://www.washingtonpost.com/world/middle-east"
        ],[
        "CNN - Asia",
        "CNN - Middle East",
        "BBC - Middle East",
        "The Economist - Chinese Economy",
        "ANN (Asia News Network) - Business News",
        "APEC (Asia-Pacific Economic Cooperation)",
        "The China Post - Business News",
        "Zawya.com - Economy & Politics News",
        "The Washington Post - Asia",
        "The Washington Post - Middle East"]]);
};

startup.prototype.news_regional_north_america = function(t) {
    t.init([[
        "http://rss.cnn.com/rss/edition_us.rss",
        "http://www.ft.com/rss/home/us",
        "http://www.ft.com/rss/canada",
        "http://www.npr.org/rss/rss.php?id=1006",
        "http://www.npr.org/rss/rss.php?id=1017",
        "http://www.bea.gov/rss/rss.xml",
        "http://www.federalreserve.gov/feeds/press_all.xml",
        "http://www.bls.gov/feed/bls_latest.rss",
        "http://feeds.reuters.com/reuters/USmarketsNews?format=xml"
        ],[
        "http://edition.cnn.com/US/",
        "http://www.ft.com/intl/world/us",
        "http://www.ft.com/intl/world/canada",
        "http://www.npr.org/",
        "http://www.npr.org/",
        "http://www.bea.gov/",
        "http://www.federalreserve.gov",
        "http://www.bls.gov/home.htm",
        "http://www.reuters.com/finance/markets/us"
        ],[
        "CNN - United States",
        "Financial Times - United States",
        "Financial Times - Canada",
        "npr.org - business",
        "npr.org - economy",
        "U.S. Bureau of Economic Analysis",
        "F.R.B. (Federal Reserve Board)",
        "U.S. Bureau of Labor Statistics - Major Economic Indicators",
        "Reuters - U.S. Market News"]]);
};

startup.prototype.news_regional_south_america = function(t) {
    t.init([[
        "http://feeds.bbci.co.uk/news/world/latin_america/rss.xml",
        "http://www.economist.com/topics/latin-american-economy/index.xml",
        "http://www.ft.com/rss/world/americas",
        "http://feeds.washingtonpost.com/rss/world/americas"
        ],[
        "http://www.bbc.co.uk/news/world/latin_america/",
        "http://www.economist.com/topics/latin-american-economy",
        "http://www.ft.com/intl/world/americas",
        "http://www.washingtonpost.com/world/americas"
        ],[
        "The Economist - Latin America economy",
        "Financial Times - Latin America & Caribbean",
        "BBC - Latin America & Caribbean",
        "The Washington Post - Americas"]]);
};

// Finance-Market_Data

startup.prototype.finance_market_data_headlines = function(t) {
    t.init([[
        "http://feeds.marketwatch.com/marketwatch/topstories/",
        "http://feeds.marketwatch.com/marketwatch/realtimeheadlines/",
        "http://feeds.marketwatch.com/marketwatch/marketpulse/",
        "http://feeds.marketwatch.com/marketwatch/bulletins"
        ],[
        "http://www.marketwatch.com/",
        "http://www.marketwatch.com/",
        "http://www.marketwatch.com/",
        "http://www.marketwatch.com/"
        ],[
        "Marketwatch.com - Top Stories",
        "marketwatch.com - Real Time Headlines",
        "Marketwatch.com - Marketpulse",
        "Marketwatch.com - Breaking"]]);
};

startup.prototype.finance_market_data_currencies = function(t) {
    t.init([[
        "http://www.dailyfx.com/feeds/forex_market_news",
        "http://www.dailyfx.com/feeds/technical_analysis",
        "http://feeds.fxstreet.com/fxstreet?format=xml"
        ],[
        "http://www.dailyfx.com",
        "http://www.dailyfx.com",
        "http://www.fxstreet.com"
        ],[
        "Forex - Market News",
        "Forex - Technical Analysis",
        "FXstreet.com - Headlines"]]);
};

startup.prototype.finance_market_data_index_based_trading = function(t) {
    t.init([[
        "http://www.ft.com/rss/ftfm/etfs",
        "http://www.indexuniverse.com/component/content/frontpage.feed",
        "http://feeds.etfdb.com/etfdb?format=xml",
        "http://etfdailynews.com/feed/",
        "http://citywire.co.uk/money/etf-news.rss"
        ],[
        "http://www.ft.com/intl/ftfm/etfs",
        "http://www.indexuniverse.com/",
        "http://etfdb.com/",
        "http://etfdailynews.com/",
        "http://citywire.co.uk/"
        ],[
        "Financial Times - Exchange traded funds and ETF data",
        "IndexUniverse - ETFs, indexes and index funds",
        "ETFdb: ETF Database",
        "etfdailynews.com - ETF Daily News",
        "Citywire Money - ETF News"]]);
};

startup.prototype.finance_market_data_mergers_and_acquisitions = function(t) {
    t.init([[
        "http://feeds.reuters.com/reuters/mergerNews?format=xml",
        "http://finance.yahoo.com/news/category-m-a/rss",
        "http://www.1888pressrelease.com/rss/mergers-acquisitions.xml",
        "http://www.just-food.com/alerts/rsssectornews.aspx?id=1048576",
        "http://www.prweb.com/rss2/nbizacquire.xml",
        "http://www.themiddlemarket.com/news/index.html?zkDo=showRSS"
        ],[
        "http://feeds.reuters.com/reuters/mergerNews",
        "http://finance.yahoo.com/news/category-m-a/",
        "http://www.1888pressrelease.com/Business-Mergers-Acquisitions-1-227.html",
        "http://www.just-food.com/sectors/mergers-acquisitions_id1048576",
        "http://www.prweb.com/newsbycategory/nbizacquire/",
        "http://www.themiddlemarket.com/"
        ],[
        "Reuters - Mergers & Acquisitions",
        "Yahoo - Mergers & Acquisitions",
        "1888pressrelease.com/ - Mergers & Acquisitions",
        "just-food.com/ - Mergers & Acquisitions in the food sector",
        "PRWeb - Mergers and Acquisitions",
        "themiddlemarket.com - Breaking"]]);
};

startup.prototype.finance_market_data_stock_spotlight = function(t) {
    t.init([[
        "http://feeds.marketwatch.com/marketwatch/stockstowatch?format=xml",
        "http://feed.zacks.com/commentary/Zacks1RankTopPerformers/rss",
        "http://feed.zacks.com/commentary/ZacksNo5Rank/rss",
        "http://feed.zacks.com/commentary/BearOfTheDay/rss",
        "http://feed.zacks.com/commentary/BullOfTheDay/rss",
        "http://feed.zacks.com/commentary/InvestmentIdeas/rss",
        "http://www.stockpickr.com/feed/today/"
        ],[
        "http://www.marketwatch.com",
        "http://www.zacks.com",
        "http://www.zacks.com",
        "http://www.zacks.com",
        "http://www.zacks.com",
        "http://www.stockpickr.com/"
        ],[
        "marketwatch.com - Stocks to Watch",
        "Zacks Investment Research - Top Performers",
        "zacks Investment Research  - Top 5  Ranking",
        "Zacks Investment Research - Bear of the day",
        "Zacks Investment Research - Bull of the day",
        "Zacks Investment Research - Investment Ideas",
        "Stockpickr: Today's Lists"]]);
};

startup.prototype.finance_market_data_upgrades_and_downgrades = function(t) {
    t.init([[
        "http://feeds.thestreet.com/tsc/feeds/rss/headlines-and-perspectives/analyst-upgrades-downgrades",
        "http://rss.briefing.com/Investor/RSS/UpgradesDowngrades.xml",
        "http://www.streetinsider.com/freefeed.php?cid=9"
        ],[
        "http://www.thestreet.com/headlines-and-perspectives/analyst-upgrades-downgrades/index.html",
        "http://www.briefing.com/investor/calendars/upgrades-downgrades/",
        "http://www.streetinsider.com/Upgrades"
        ],[
        "Thestreet.com",
        "Briefing.com",
        "Streetinsider.com"]]);
};

startup.prototype.finance_market_data_sector_updates = function(t) {
    t.init([[
        "http://feeds.reuters.com/reuters/basicmaterialsNews",
        "http://www.ft.com/rss/companies/energy",
        "http://feeds.reuters.com/reuters/USenergyNews",
        "http://feeds.reuters.com/reuters/environment",
        "http://www.ft.com/rss/companies/finances",
        "http://feeds.reuters.com/reuters/financialsNews",
        "http://www.ft.com/rss/companies/healthcare",
        "http://feeds.reuters.com/reuters/UShealthcareNews",
        "http://feeds.reuters.com/reuters/industrialsNews",
        "http://www.ft.com/rss/companies/support-services",
        "http://www.ft.com/rss/companies/technology",
        "http://feeds.reuters.com/reuters/technologysectorNews",
        "http://feeds.reuters.com/reuters/telecomsectorNews",
        "http://www.ft.com/rss/companies/transport",
        "http://www.ft.com/rss/companies/utilities",
        "http://feeds.reuters.com/reuters/utilitiesNews"
        ],[
        "http://www.reuters.com/finance/markets",
        "http://www.reuters.com/finance/markets",
        "http://www.ft.com/intl/companies/energy",
        "http://www.reuters.com/finance/markets",
        "http://www.ft.com/companies/finances",
        "http://www.reuters.com/finance/markets",
        "http://www.ft.com/companies/healthcare",
        "http://www.reuters.com/finance/markets",
        "http://www.reuters.com/finance/markets", 
        "http://www.ft.com/companies/support-services",
        "http://www.ft.com/companies/technology",
        "http://www.reuters.com/finance/markets",
        "http://www.reuters.com/finance/markets",
        "http://www.ft.com/companies/transport",
        "http://www.ft.com/companies/utilities",
        "http://www.reuters.com/finance/markets"
        ],[
        "Reuters - Basic Materials Sector",
        "Financial Times - Energy Sector",
        "Reuters - Energy Sector",
        "Reuters - Environmental  Sector",
        "Reuters - Financial Sector",
        "Financial Times - Financial Sector",
        "Financial Times - Healthcare Sector",
        "Reuters - Healthcare Sector",
        "Reuters - Industrial Sector",
        "Financial - Services Sector",
        "Financial Times - Technology",
        "Reuters - Technology Sector",
        "Reuters - Telecom Sector",
        "Financial Times - Transport Sector",
        "Financial Times - Utilities",
        "Reuters - Utilities"]]);
};

// Investing - Analysis and Insights

startup.prototype.finance_investing_analysis_and_insights_blogs = function(t) {
    t.init([[
        "http://anthony-catanach.squarespace.com/blog?format=rss",
        "http://markdow.tumblr.com/rss",
        "http://interloping.com/feed/",
        "http://feeds.feedburner.com/bclund?format=xml",
        "http://inpursuitofvalue.wordpress.com/feed/",
        "http://www.forbes.com/sites/ariweinberg/feed/"
        ],[
        "http://grumpyoldaccountants.com/",
        "http://markdow.tumblr.com/",
        "http://interloping.com/",
        "http://bclund.com/",
        "http://inpursuitofvalue.wordpress.com/",
        "http://www.forbes.com/sites/ariweinberg/"
        ],[
        "Tony Catanach - Professor",
        "Mark Dow - Money Manager",
        "Anonymous Former Investment Banker",
        "Brian Lund - Entrepreneur",
        "Simon Lack - Investment Advisor",
        "Ari Weinberg - Writer"]]);
};

startup.prototype.finance_investing_analysis_and_insights_articles = function(t) {
    t.init([[
        "http://www.businessweek.com/rss/bwdaily.rss",
        "http://feeds.fool.com/usmf/foolwatch",
        "http://feed.zacks.com/commentary/VoiceOfThePeople/rss",
        "http://citywire.co.uk/money/latest-news.rss"
        ],[
        "http://www.BusinessWeek.com",
        "http://www.fool.com",
        "http://www.zacks.com",
        "http://citywire.co.uk/money"
        ],[
        "BusinessWeek.com",
        "Fool.com - The Motley Fool",
        "Zacks Investment Research - Voice of the People",
        "Citywire Money - Latest News"]]);
};
