/*
*******************************************************************************
** popup.js                                                  author: GMacciocu
** (c) Copyright 2011-2013 - All Rights Reserved
** 
** notes:
** 
** - include css file popup.css
** - Call function popup.write !ONLY ONCE! from the index page
**
*******************************************************************************
*/

var popup = {
    // popup.Close
	Close: function() {
    
        var fp = fw.ui.fade.Out;
        var fp_params = {
            ID: "popup",
            opacity: 1,
            clean: true
        };
    
        // fade-out animations (performs disable at the end of fade-out)
        fw.ui.fade.Out("popup-page-disable", 0.6, false, fp, fp_params);
        /*fw.ui.fade.Out("popup", 1, true);*/
	},
    
    // popup.Onmouseover
	Onmouseover: function() {
		$("#popup_id_temp")[0].style.cursor = "pointer";
    },
    
    // popup.wr
    wr: function(width) { 
        var height = 600; 
        // get available height
        if (window.screen != null)
            height = window.screen.availHeight; 
        else if (window.innerHeight != null)
            height =   window.innerHeight; 
        else if (document.body != null)
            height = document.body.clientHeight;
        // apply margin and contvert to string
        height = parseInt(height / 3 * 2) + "px";
        // output html
        $("#popup").html(
            '<table style="width:'+width+'">'
                +'<tr class="header">'
                    +'<td class="td-l" id="id_popup_header_title">'
                    +'</td>'
                    +'<td class="td-r">'
                        +'<a href="#" onclick="popup.Close();">'
                            +'<img src="./img/icons_white_32x32/round_delete.png" height="23px" width="23px">'
                        +'</a>'
                    +'</td>'
                +'</tr>'
                +'<tr class="content">'
                    +'<td colspan="2">'
                        +'<div id="id_popup_content" style="max-height:'+height+'; overflow-x:hidden; overflow-y:auto;"></div>' /* dynamicly generated content here */
                    +'</td>' 
                +'</tr>'
            +'</table>');
    },
    
    /** 
     * popup.create
     * @param
     * var popup_params = {
     *     header_content : "header content here",
     *     main_content : "main content here",
     *     width : ''
     * }
     */
	create: function(popup_params) {
        // output the popup content holder
        this.wr(popup_params.width);
        // init popup content
        $("#id_popup_header_title").html(popup_params.header_content);
		$("#id_popup_content").html(popup_params.main_content);
        // enable + fade-in animations
        fw.ui.multi.en(["popup-page-disable","popup"], true);
        fw.ui.fade.In("popup-page-disable", 0, 0.6);
        fw.ui.fade.In("popup", 0, 1);
	}
}