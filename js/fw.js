/*
*******************************************************************************
** fw.js (framework)                                               
** (c) Copyright 2011-2013 - All Rights Reserved
** author: GMacciocu
*******************************************************************************
**
** fw.color.getStatus
** 
** fw.str.trimTail
** fw.str.setChar
** 
** fw.ui.single.en
** fw.ui.multi.en
** fw.ui.hover
** 
** fw.date.getDayNrOfWeek
** fw.date.getDayNrOfMonth
** fw.date.getMonthAbrv
** fw.date.getTimeFormat
** fw.date.get
**
*******************************************************************************
*/

/**
 * fw : My Framework
 *
 * fw.color.getStatus
 *
 * fw.str.trimTail
 * fw.str.setChar
 *
 * fw.ui.fade.In
 * fw.ui.fade.Out
 * fw.ui.single.en
 * fw.ui.multi.en
 * fw.ui.hover
 *
 * fw.date.getDayNrOfWeek
 * fw.date.getDayNrOfMonth
 * fw.date.getMonthAbrv
 * fw.date.getTimeFormat
 * fw.date.get
 */
var fw = {
    // onkeypress related methods
    onkeypress: {
        /**
         * fw.onkeypress.numbersOnly
         */
        numbersOnly: function(e, t) {
            var k = (e.keyCode ? e.keyCode : e.which);
        
            if (k>=48 && k<=57 ||          // numbers
                k==8  || k==9  || k==13 || // Backspace and Tab and Enter
                k==35 || k==36 ||          // Home and End
                k==37 || k==39 ||          // left and right arrows
                k==46 || k==45) {          // Del and Ins
                // input VALID
                return true;
            } else {
                // input INVALID
                return false;
            }
        }
    },

    // color related methods
    color: {
        wh: "#FFFFFF",
        rd: "#D10A0A",
        gr: "#3FD10A",
        bl: "#749DDE",
        
        /**
         * fw.color.getStatus
         */
        getStatus: function(v) {
            if (parseFloat(v) > 0)
                return this.gr; // ok
            else if (parseFloat(v) < 0)
                return this.rd; // err
            else
                return this.bl; // info
        }
    },
    
    // string related methods
    str: {
        /**
         * fw.str.trimTail
         * strim the length of string 's' downto 'max' characters
         */
        trimTail: function(s, max) {
            return ((s.length > max) ? s = s.substring(0, max) + '...' : s);
        },
        
        /**
         * fw.str.setChar
         * return given string 's' with a replaced given character 'c' at given position 'i'
         */
        setChar: function(s, i, c) {
            return ((i > s.length-1) ? s : s.substr(0, i) + c + s.substr(i + 1));
        }
    },
    
    // user-interface related methods
    ui: {
        fade: {
            TRANSITION_INTERVAL: 20,
            busy_fade_in: false,
            
            /**
             * fw.ui.fade.In
             */
            In: function(ID, opacity, opacity_dark) {
                fw.ui.fade.busy_fade_in = true;
                $("#"+ID)[0].style.opacity = opacity;
                
                if (opacity < opacity_dark) {
                    setTimeout(function() { 
                        fw.ui.fade.In(ID, (opacity + 0.1), opacity_dark); 
                    }, fw.ui.fade.TRANSITION_INTERVAL);
                } else {
                    fw.ui.fade.busy_fade_in = false;
                }
            },
            
            /**
             * fw.ui.fade.Out
             */
            Out: function(ID, opacity, clean, fp, fp_params) {
                if (fw.ui.fade.busy_fade_in == false) {
                    $("#"+ID)[0].style.opacity = opacity;
                    
                    if (opacity > 0) {
                        setTimeout(function() { 
                            fw.ui.fade.Out(ID, (opacity - 0.1), clean, fp, fp_params);
                        }, fw.ui.fade.TRANSITION_INTERVAL);
                    } else {
                        fw.ui.single.en(ID, false);
                        
                        if (clean == true)
                            $("#"+ID).html("");
                            
                        if (fp)
                            fp(fp_params.ID, fp_params.opacity, fp_params.clean, false, false);
                    }
                }
            }
        },
    
        single: {
            /**
             * fw.ui.single.en
             */
            en: function(ID, en) {
                switch (en) {
                    case false:
                        $("#"+ID).css("visibility", "hidden");
                        $("#"+ID).css("display", "none");                        
                        break;
                    case true:
                        $("#"+ID).css("visibility", "visible");
                        $("#"+ID).css("display", "inline");
                        break;
                }
            }
        },        
        
        multi: {
            /**
             * fw.ui.multi.en
             */
            en: function(ID, en) {
                switch (en) {
                    case false:
                        for(var i=0; i<ID.length; i++) {
                            $("#"+ID[i])[0].style.visibility = "hidden";
                            $("#"+ID[i])[0].style.display = "none";
                        }    
                        break;
                    case true:
                        for(var i=0; i<ID.length; i++) {
                            $("#"+ID[i])[0].style.visibility = "visible";
                            $("#"+ID[i])[0].style.display = "inline";
                        }                
                        break;
                }
            }
        },
        
        /** 
         * fw.ui.hover
         */
        hover: function(ID, bgColor) {
            $("#"+ID)[0].style.cursor = "pointer";
            if (bgColor != undefined) { $("#"+ID)[0].style.background = bgColor; }
        }
    },
    
    // ajax object (xmlhttp) related methods
    xmh: {
        /**
         * fw.xmh.ok
         */
        ok: function(xmh) {
            if (xmh.readyState == 4 && xmh.status == 200)
                return true;
            else
                return false;
        },
        
        /**
         * fw.xmh.getObj
         * @return xmlhttp (ajax) object
         */
        getObj: function() {
            var req = false;
            try {
                req = new XMLHttpRequest(); // for example, Firefox
            } catch(err1) {
                try {
                    req = new ActiveXObject("Msxml2.XMLHTTP"); // some versions of IE
                } catch(err2) {
                    try {
                        req = new ActiveXObject("Microsoft.XMLHTTP"); // some versions of IE
                    } catch(err3) {
                        alert("failed to create xmlhttp (ajax) object");
                        req = false;
                    }
                }
            }
            return req;
        }
    },
    
    // date & time related methods
    date: {
        ABRV_DAYS: [
            'Sun', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat'],
        ABRV_MONTHS: [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        
        /** 
         * fw.date.getDayNrOfWeek
         * return day of the week (string abreviation) 
         * in function of x amount of added / sutracted days
         */
        getDayNrOfWeek: function(currentDay, shift) {
            var newDay = currentDay;
            
            for (var i=0; i<7; i++) {
                if (currentDay == this.ABRV_DAYS[i]) {
                    var temp_abrv_days = this.ABRV_DAYS;
                    temp_abrv_days.move((i + shift) % 7);
                    newDay = temp_abrv_days[i];
                    break;
                }
            }
            return newDay;
        },
        
        /**
         * fw.date.getDayNrOfMonth
         * Returns day number according x amount of added / subtracted (MAX.31) days
         *
         * Each "knuckle" month has 31 days, while each "valley" 
         * month has only 30 days (excepting of course February).
         *
         * 30 days hath September, April, June and November,
         * All the rest have 31, Excepting February alone
         * (And that has 28 days clear, With 29 in each leap year).  
         *
         * dayNr: integer day number
         * month: month (string) abreviation (Jan, Feb, Mar..)
         * leap: true => leap year, false => not leap year
         * shift: amount of day to be added / subtracted
         */    
        getDayNrOfMonth: function(dayNr, month, leap, shift) {
            var retDayNr = 0;
            var monthsDayCount = new Array(
                31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
            
            if (leap == true)
                monthsDayCount[1] = 29;
            
            dayNr += shift;
            
            for (var i=0; i<12; i++) {
                if (this.ABRV_MONTHS[i] == month) {
                    if (dayNr > monthsDayCount[i]) {
                        while(dayNr > monthsDayCount[i]) {
                            dayNr--;
                            retDayNr++;
                        }
                    } else {
                        retDayNr = dayNr;
                    }
                }
            }
            return retDayNr;
        },
        
        /**
         * fw.date.getDayAbrv
         * @param nr i 0..6
         */
        getDayAbrv: function(i) {
            return ((i >=0 && i <=6 && i%1 == 0) ? this.ABRV_DAYS[i] : "ERR");
        },
        
        /**
         * fw.date.getMonthAbrv
         * @param nr i 0..11
         */
        getMonthAbrv: function(i) {
            return ((i >=0 && i <=6 && i%1 == 0) ? this.ABRV_MONTHS[i] : "ERR");
        },
        
        /**
         * fw.date.getMonthAbrvShift
         * return day of the month (string abreviation) in function of
         * x amount of added / subtracted months 'shift'
         */
        getMonthAbrvShift: function(currentMonth, shift) {
            var newMonth = currentMonth;
            for (var i=0; i<12; i++) {
                if (currentMonth == this.ABRV_MONTHS[i]) {
                    var temp_abrv_months = this.ABRV_MONTHS;
                    temp_abrv_months.move((i + shift) % 12);
                    newMonth = temp_abrv_months[i];
                    break;
                }
            }
            return newMonth;
        },
        
        /**
         * fw.date.getTimeFormat
         * formats to 0x:0x:0x format (3 x 2 digit)
         */
        getTimeFormat: function(hr, min, sec) {
            var time = "";
            
            if (hr < 10)
                time += '0' + hr +":";
            else
                time += hr +":";
        
            if (min < 10)
                time += '0' + min +":";
            else
                time += min +":";
        
            if (sec < 10)
                time += '0' + sec;
            else
                time += sec;
                
            return time;
        }
    }
};