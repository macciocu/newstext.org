/******************************************************
* jQuery plug-in
* Adjustment of Easy Pinned Footer
* Developed by J.P Given (http://johnpatrickgiven.com)
* Useage: anyone so long as credit is left alone
******************************************************/
(function(jQuery) {
  // plugin definition
  jQuery.fn.pinHeader = function(options) {	
  // Get the height of the footer and window + window width
  var wH = jQuery(window).height();
  var wW = getWindowWidth();
  var fH = jQuery(this).outerHeight(true);
  var bH = jQuery("body").outerHeight(true);
  var mB = parseInt(jQuery("body").css("margin-bottom"));
  
  if (options == 'relative') {
    if (bH > getWindowHeight()) {
      jQuery(this).css("position","absolute");
      jQuery(this).css("width",wW + "px");
      jQuery(this).css("top",bH - fH + "px");
      jQuery("body").css("overflow-x","hidden");
    } else {
      jQuery(this).css("position","fixed");
      jQuery(this).css("width",wW + "px");
      /*jQuery(this).css("top",wH - fH + "px");*/
      jQuery(this).css("top","0px");
    }
  } else { // Pinned option
      // Set CSS attributes for positioning footer
      jQuery(this).css("position","fixed");
      jQuery(this).css("width",wW + "px");
      /*jQuery(this).css("top",wH - fH + "px");*/
      jQuery(this).css("top","0px");
      jQuery("body").css("height",(bH + mB) + "px");
    }
  };
  
  // private function for debugging
  function debug(jQueryobj) {
    if (window.console && window.console.log) {
      window.console.log('Window Width: ' + jQuery(window).width());
      window.console.log('Window Height: ' + jQuery(window).height());
    }
  };
  
  // Dependable function to get Window Height
  function getWindowHeight() {
    var windowHeight = 0;
    if (typeof(window.innerHeight) == 'number') {
      windowHeight = window.innerHeight;
    } else {
      if (document.documentElement && document.documentElement.clientHeight) {
        windowHeight = document.documentElement.clientHeight;
      } else {
        if (document.body && document.body.clientHeight) {
          windowHeight = document.body.clientHeight;
        }
      }
    }
    return windowHeight;
  };
  
  // Dependable function to get Window Width
  function getWindowWidth() {
    var windowWidth = 0;
    if (typeof(window.innerWidth) == 'number') {
      windowWidth = window.innerWidth;
    } else {
      if (document.documentElement && document.documentElement.clientWidth) {
        windowWidth = document.documentElement.clientWidth;
      } else {
        if (document.body && document.body.clientWidth) {
          windowWidth = document.body.clientWidth;
        }
      }
    }
    return windowWidth;
  };
})(jQuery);