<?php
date_default_timezone_set('UTC');

$filename = 'log.txt';

if (is_writable($filename)) {
    // open file in append mode.
    if (!$handle = fopen($filename, 'a')) {
         echo "Cannot open file ($filename)";
         exit;
    }
 
    $mssg = "\n".date(DATE_RFC822)."#".$_POST['mssg'];

    // Write $mssg to our opened file.
    if (fwrite($handle, $mssg) === FALSE) {
        echo "Cannot write to file ($filename)";
        exit;
    }

    fclose($handle);
} else {
    echo "The file $filename is not writable";
}
?>
