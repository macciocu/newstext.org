<?php
/******************************************************************************
** Pausing RSS Scroller bridge script
** Author: Dynamic Drive (http://www.dynamicdrive.com)
** Created: March 16th, 2006.
** Function: Converts requested RSS feed from lastRSS into JavaScript array
******************************************************************************
*/

/*date_default_timezone_set('America/Los_Angeles');*/
date_default_timezone_set('UTC');

// include lastRSS
include "rssLast.php"; //path to lastRSS.php on your server relative to scrollerbridge.php

// Create lastRSS object
$rss = new lastRSS;
$rss->cache_dir = 'cache'; //path to cache directory on your server relative to scrollerbridge.php. Chmod 777!
$rss->date_format = 'U';
                                    
$rss->cache_time = 1800; //Global cache time before fetching RSS feed again, in seconds.

class RssFeeder {
    
    /* PRIVATE METHODS */

    // cleanup text to html UTF-8
    private function clean_text($text, $length=0) {
        $html = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        $text = strip_tags($html);
    
        if ($length>0 && strlen($text)>$length) {
            $cut_point=strrpos(substr($text, 0, $length), ' ');
            $text = substr($text, 0, $cut_point) . '…';
        }
        
        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        return $text;
    }

    // encode text
    private function slashit($what) { 
        $newstring = str_replace('&apos;','\'',$what); 
        $newstring = str_replace('(#)','[#]',$newstring);
        $newstring = str_replace('(##)','[##]',$newstring);    
        $newstring = $this->clean_text($newstring, strlen($newstring));
        return rawurlencode($newstring);
    }
    
    /* PUBLIC METHODS */
    
    function outputRSS($url, $max_items) {
        global $rss;   
        
        $rv = "error";
        $rs = $rss->Get($url);
        
        if ($rs) {
            $rv = "";
            $item_i = 0;
        
            foreach($rs['items'] as $item) {   
                $rv .= $this->slashit($item[link])."(#)"
                    .$this->slashit($item[title])."(#)"
                    .$this->slashit($item[description])."(#)"
                    .$this->slashit($item[pubDate])."(#)"
                    .$this->slashit(date(DATE_RFC822))."(##)";
            
                if (++$item_i >= $max_items) { break; }
            }
            
            $rv = substr($rv, 0, strlen($rv) - 4); // remove the last "(##)"
        
        } else {
            $rv = "rssFeeder->outputRSS:\"Sorry: It's not possible to reach RSS file $url\"";
        }
        echo $rv;
    }
}

$rssid = $_GET['rssid'];
$rssurl = $_GET['url'];
$maxItems = (int)$_GET['maxitems'];

$rssFeeder = new RssFeeder();
$rssFeeder->outputRSS($rssurl, $maxItems);
?>