<script type="text/javascript">
/*<![CDATA[*/

/*
*******************************************************************************
** rssTracker.inc.php                                        author: GMacciocu
** (c) Copyright 2011-2013 - All Rights Reserved
*******************************************************************************
*/

/**
 * @param : feed titles, feed rss entry url's, feed website link
 */
function RssTracker(ttl, url, lnk) {
    // make the input parameters part of the object
    this.ttl = ttl, this.url = url, this.lnk = lnk;
    // latest - index, delta minutes, link, title, description, publication date
    this.latest = { i:0, url:[], ttl:[], dcr:[], pbl:[] };
    // PHP AXJAX URL
    this.PHPURL = 'php/rssFeeder.php?';
    // array indexes (NB: don't change index numbers without reviewing the code!)
    this.I = { URL:0, TTL:1, DCR:2, HDR:3, SRC:4 };
    // string seperators
    this.SPR = { ANLS:"(###)", FEED:"(##)", MSSG:"(#)" };        
    // min / max limits
    this.LIMIT = {
        HOTNEWS:  120, // Breaking news treshold [minutes]
        MAXFEEDS: 100,
        MAXITEMS: 23,  // Max. nr. of rss that will be extracted from source
        TXTSHORT: 250,
        TXTLONG:  1024
    };      
    // $(id)
    this.JQID = {
        LATEST:       '#content1',
        HEADLINES:    '#content2',
        SEARCH_RES:   '#content3',
        SCROLLERS:    '#content_rssinstances',
        SEARCH_TAB:   '#tab3'
    };
    
    this.nr_of_feeds = (url.length < this.LIMIT.MAXFEEDS) ? url.length : this.LIMIT.MAXFEEDS;
    
    this.rssrx = new Array();
    this.nr_of_entries = new Array();
    
    // create 2D array
    for (var feed_i=0; feed_i<this.nr_of_feeds; feed_i++) {
        this.rssrx[feed_i] = new Array(2);
    }
    
    var html = '', bttn_back = '', bttn_next = '', bttn_doc = '';
    
    for (var feed_i=0; feed_i<this.nr_of_feeds; feed_i++) {        
        this.scroller.Id[feed_i] = "id_scroller_" + feed_i;
        this.scroller.feed_i[feed_i] = 0;
        this.nr_of_entries[feed_i] = 0;
         
        bttn_doc = 
            '<a href="#" onclick="rssTracker.doc('+feed_i+');">'
                +'<img src="./img/icons_white_16x16/rss.png" class="lnk" height="16" width="16"/>'
            +'</a>';
            
        bttn_back =
            '<a href="#" onclick="rssTracker.scroller.onclick.back(\''+this.scroller.Id[feed_i]+'\','+feed_i+', rssTracker);">'
                +'<img src="./img/icons_white_16x16/br_prev.png" class="lnk-xtr-hor-marg" height="16" width="16"/>'
            +'</a>';
        
        bttn_next =
            '<a href="#" onclick="rssTracker.scroller.onclick.next(\''+this.scroller.Id[feed_i]+'\','+feed_i+', rssTracker);">'
                +'<img src="./img/icons_white_16x16/br_next.png" class="lnk-xtr-hor-marg" height="16" width="16"/>'
            +'</a>';
         
        html +=
            '<div class="hidden" id="id_scroller_outline_'+this.scroller.Id[feed_i]+'">'
                +'<table class="scroller_x">'
                    +'<tr class="rss_inner_top">'
                        +'<td class="bttn_doc">'
                            +bttn_doc
                        +'</td>'
                        +'<td class="str_header">'
                            +fw.str.trimTail(this.ttl[feed_i], 64)
                        +'</td>'
                        +'<td class="sources" id="tbltd-sources_'+this.scroller.Id[feed_i]+'">'
                        +'</td>'
                        +'<td class="bttn_back">'
                            +bttn_back
                        +'</td>'
                        +'<td class="bttn_next">'
                            +bttn_next
                        +'</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td class="rss_inner_bottom" colspan="5">'
                            +'<div id="'+this.scroller.Id[feed_i]+'"></div>'
                        +'</td>'
                    +'</tr>'
                +'</table>'
            +'</div>';
	}    
    
    $(this.JQID.SCROLLERS).html(html);    
    
    for (feed_i=0; feed_i<this.nr_of_feeds; feed_i++) {
        this.getRssData(feed_i);
    }
    
    <?php
    if ($admin) {
    ?>
        var t = this;
        setTimeout(function() { 
            t.Admin.checkFeedLoad(t);
        }, 10000);
    <?php
    }
    ?>
}

/** 
 * perform ajax GET request
 * @info http://api.jquery.com/jQuery.get/
 */
RssTracker.prototype.getRssData = function(feed_i) {
    var rssfeed = this;
    
    $.ajax({ 
        type: "GET",
        async: true,
        url: 
            this.PHPURL 
            + "rssid=" + this.ttl[feed_i]
            + "&url=" + this.url[feed_i]
            + "&maxitems=" + this.LIMIT.MAXITEMS
            + "&n=" + parseInt(Math.random() * 99999999), 
        
        success: function(responseText) {
            rssfeed.initContent(feed_i, responseText); 
        }
    }); 
};

<?php
if ($admin) {
?>
    RssTracker.prototype.Admin = {
        feed_init_count: 0,
        feed_init_ok: "",
        
        logWr: function(str) {
            $.ajax({
                type: "POST",
                url: "php/logHandler.php",
                data: { mssg: str, n: parseInt(Math.random() * 99999999) }
            }).done(function(resp) {
                alert("Data Saved: " + resp);
            });
        },
        
        /**
         * if unloaded feeds write the following to log.txt:
         * nr. of loaded feeds/total nr. of feeds/corrupt_url_1#corrupt_url_2 .. etc.
         */
        checkFeedLoad: function(t) {
            if (this.feed_init_count != t.nr_of_feeds) {
                var url_err = "";
                var feed_ok = (this.feed_init_ok.substring(0, this.feed_init_ok.length - 1)).split("-");
                
                for (var feed_i=0; feed_i<t.nr_of_feeds; feed_i++) {
                    if (parseInt(feed_ok[feed_i]) != feed_i)
                        url_err += t.url[feed_i] + "#";
                }
                this.logWr(this.feed_init_count + "/" +t.nr_of_feeds
                    + "/" + url_err.substring(0, url_err.length - 1));
            }
        }
    };
<?php
}
?>

/**
 * extract and format the rss data
 */
RssTracker.prototype.initContent = function(feed_i, ajax_rx) {
    if (ajax_rx != "" && feed_i < this.nr_of_feeds) {
        var feed_data = ajax_rx.split(this.SPR.FEED);
        
        this.nr_of_entries[feed_i] = feed_data.length;
        
		for (var entry_i=0; entry_i<this.nr_of_entries[feed_i]; entry_i++) {
            this.rssrx[feed_i][entry_i] = feed_data[entry_i].split(this.SPR.MSSG);
            
            this.rssrx[feed_i][entry_i][this.I.HDR] = this.getPublicationInfo(
                feed_i, parseInt(this.rssrx[feed_i][entry_i][this.I.HDR]));
            
            this.rssrx[feed_i][entry_i][this.I.URL] = unescape(this.rssrx[feed_i][entry_i][this.I.URL]);
            this.rssrx[feed_i][entry_i][this.I.TTL] = unescape(this.rssrx[feed_i][entry_i][this.I.TTL]);
            this.rssrx[feed_i][entry_i][this.I.DCR] = unescape(this.rssrx[feed_i][entry_i][this.I.DCR]);
            
            // replace urls with links
            this.rssrx[feed_i][entry_i][this.I.DCR] = this.rssrx[feed_i][entry_i][this.I.DCR].replace(
                /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig,
                '<a href="$1" target="_blank">link</a>');
            
            this.latest.url[this.latest.i] = this.rssrx[feed_i][entry_i][this.I.URL];
            this.latest.ttl[this.latest.i] = this.rssrx[feed_i][entry_i][this.I.TTL];
            this.latest.dcr[this.latest.i] = this.rssrx[feed_i][entry_i][this.I.DCR];
            this.latest.pbl[this.latest.i] = this.rssrx[feed_i][entry_i][this.I.HDR];
            
            // if first time here
            if (this.latest.i++ == 0) {
                $(this.JQID.HEADLINES).html("");
            }
            
            // if top rss entry
            if (entry_i == 0) {
                $(this.JQID.HEADLINES).append(this.rssContentDisplay(this.rssrx[feed_i][entry_i], [0,0,0,0]));
            }
		}
        
        this.scroller.init(feed_i, this);
        this.initLatest();
        
        <?php
        if ($admin) {
        ?>
            this.Admin.feed_init_count++;
            this.Admin.feed_init_ok += feed_i + "-";
        <?php
        }
        ?>
        
        var jqId = "#id_scroller_outline_" + this.scroller.Id[feed_i];
        $(jqId).css("visibility", "visible");
        $(jqId).css("display", "block");
	}
};

/**
 * @return rss channel title + article publication date
 * e.g. : marketwatch.com - Real Time Headlines | Thu Apr 18 2013 09:06:17 (8 min. ago)
 */
RssTracker.prototype.getPublicationInfo = function(feed_i, publication_date) {
    var dt1 = new Date();
    var dt2 = new Date();
    var time = parseInt(dt2.getTime());
	var dTime = time - 1000 * publication_date;
	
    dt1.setTime(time - dTime);
    
    var date =
        fw.date.getDayAbrv(dt1.getDay())
        + " " + fw.date.getMonthAbrv(dt1.getMonth()) + " "
        + dt1.getDate() + " "
        + dt1.getFullYear() + " "
        + fw.date.getTimeFormat(dt1.getHours(), dt1.getMinutes(), dt1.getSeconds());
        
        
    var dMin = parseInt(dTime / 1000 / 60); // delta minutes
	var min = dMin % 60;
	var hr = (dMin - min) / 60;

    var pbinfo = new Array();
    
    var pbinfo = {
        // publication date
        pbl: 
            '<span class="rss_catg">'
                +date
            +'</span>&nbsp;'
            +'<span class="'+((dMin <= this.LIMIT.HOTNEWS) ? "rss_breaking": "rss_notbreaking")+'">'
                +'('+((hr >= 1) ? (hr + '.' + min + ' hr. ago') : (min + ' min. ago'))+')'
            +'</span>',
        // source | publication date
        src_pbl:
            '<span class="rss_catg">'
                + this.ttl[feed_i] + '&nbsp;|&nbsp;'+ date
            +'</span>&nbsp;'
            +'<span class="'+((dMin <= this.LIMIT.HOTNEWS) ? "rss_breaking": "rss_notbreaking")+'">'
                +'('+((hr >= 1) ? (hr + '.' + min + ' hr. ago') : (min + ' min. ago'))+')'
            +'</span>',
        // delta minutes
        delta_min: dMin
    }
    
    return pbinfo;
};

/**
 * return formatted html content for the latest entries section
 */
RssTracker.prototype.initLatest = function() {
	var j = 0;
    var temp = new Array();
    var mssg = new Array();
    
    // initialise latest entries
    // 
    // sort in ascending order (insertation sort algorithm)
    // (http://en.wikipedia.org/wiki/Insertion_sort)
    // 
    // The collection is sorted from beginning to end.
    // While looping over the elements, it compares the current
    // element to the ones before it in the collection. All the
    // elements that are bigger than the current element are moved
    // one place to the right. The current element is inserted after
    // the first element that is smaller than itself.
    //
	for (var i=1; i<this.latest.i; i++) {
		temp[0] = this.latest.url[i];
		temp[1] = this.latest.ttl[i];
		temp[2] = this.latest.dcr[i];
		temp[3] = this.latest.pbl[i];
		j = i - 1;
		while(j >= 0 && this.latest.pbl[j].delta_min > temp[3].delta_min) {
			this.latest.url[j+1] = this.latest.url[j];
			this.latest.ttl[j+1] = this.latest.ttl[j];
			this.latest.dcr[j+1] = this.latest.dcr[j];
			this.latest.pbl[j+1] = this.latest.pbl[j];
			j = j - 1;
		}
		this.latest.url[j+1] = temp[0];
		this.latest.ttl[j+1] = temp[1];
		this.latest.dcr[j+1] = temp[2];
		this.latest.pbl[j+1] = temp[3];
	}
    
    $(this.JQID.LATEST).html("");
    for (var i=0; i<this.latest.i; i++) {
        mssg[0] = this.latest.url[i];
        mssg[1] = this.latest.ttl[i];
        mssg[2] = this.latest.dcr[i];
        mssg[3] = this.latest.pbl[i];
        $(this.JQID.LATEST).append(this.rssContentDisplay(mssg, [0,0,0,0]));
    }
};

/**
 * @param mssg: ajax obtained content for the related rssfeed
 * @param trim: array according the same structure as mssg, indicating
 *              if any of the strings need to be trimmed 
 * @return htm formatted rss content display
 */
RssTracker.prototype.rssContentDisplay = function(mssg, trim, pbl_type) {
    var publication_info = mssg[this.I.HDR].src_pbl;
    
    if (typeof pbl_type != null && typeof pbl_type != undefined)
        if (pbl_type == "pbl")
            publication_info = mssg[this.I.HDR].pbl
    
    return(
        '<a target="_blank" href="'+mssg[this.I.URL]+'">'
            +((trim[this.I.TTL]) ? 
                fw.str.trimTail(mssg[this.I.TTL], this.LIMIT.TXTSHORT) : 
                fw.str.trimTail(mssg[this.I.TTL], this.LIMIT.TXTLONG))
        +'</a><br/>'
        +publication_info+'<br/>'
        +((trim[this.I.DCR]) ? 
            fw.str.trimTail(mssg[this.I.DCR], this.LIMIT.TXTSHORT): 
            fw.str.trimTail(mssg[this.I.DCR], this.LIMIT.TXTLONG))
        +'<br/><br/>');
};

/**
 * @brief create rss feed list popup
 * @param the feed_ied rss feed
 */
RssTracker.prototype.doc = function(feed_i) {
    var popup_params = {
        header_content : this.ttl[feed_i],
        main_content : "",
        width : "80%"
    };
    
    fw.ui.single.en("popup-page-disable", true);
    
    for (var entry_i=0; entry_i<this.nr_of_entries[feed_i]; entry_i++) {
        popup_params.main_content +=
            '<div width="100%">'
                +'<div class="rss_title" style="overflow:hidden; height:1.2em">'
                    +'<a href="'+this.rssrx[feed_i][entry_i][this.I.URL]+'">'
                        +this.rssrx[feed_i][entry_i][this.I.TTL]
                    +'</a>'
                +'</div>'
                +'<span class="rss_date">'
                    +this.rssrx[feed_i][entry_i][this.I.HDR].src_pbl
                +'</span><br/>'
                +'<div class="rss_descr">'
                    +this.rssrx[feed_i][entry_i][this.I.DCR]
                +'</div>'
            +'</div><br/>';
    }
    
    setTimeout(function() {
        popup.create(popup_params);
    }, 250);
};

/**
 * scroller functionallity, json
 */
RssTracker.prototype.scroller = {
    feed_i:[],
    Id:[],
    
    // this.scroller.init
    init: function(feed_i, t) {
        var entry_i = this.feed_i[feed_i];
      
        $("#"+this.Id[feed_i]).html(t.rssContentDisplay(t.rssrx[feed_i][entry_i], [1,1,1,1], "pbl"));
        $("#tbltd-sources_" + this.Id[feed_i]).html((entry_i + 1) + '/' + t.nr_of_entries[feed_i]);
        
        return (t.nr_of_entries[feed_i]);
    },
    
    onclick: {
        // this.scroller.onclick.next
        next: function(scrollerID, feed_i, t) {
           if (t.scroller.feed_i[feed_i] < (t.scroller.init(feed_i, t) - 1))
                t.scroller.feed_i[feed_i]++;
            else
                t.scroller.feed_i[feed_i] = 0;
            
            t.scroller.init(feed_i, t);
        },
        
        // this.scroller.onclick.back
        back: function(ScrollerID, feed_i, t) {
            if (t.scroller.feed_i[feed_i] > 0)
                t.scroller.feed_i[feed_i]--;
            else
                t.scroller.feed_i[feed_i] = t.scroller.init(feed_i, t) - 1;;
            
            t.scroller.init(feed_i, t);
        }
    }
};

/**
 * search for keywords as given in $("txt-search").value
 * output results to $("tbltd-search_results").innerHTML
 */
RssTracker.prototype.search = function(JQID) {
    var k = 0; 
    var kMem = 0;
    var str = "";
    var temp = "";
    var detect = false;
    var feedback = "";
    var str_lowercase = "";
    var cd = new Array(); // descriptions
    var cl = new Array(); // corresponding links of the split up words
    var duplicates = new Array(); // tracks duplicate entries    
    var search_str_arr = ($(JQID).val() + " /").toLowerCase().split(" ");
    
    // search algorithm
    for (var feed_i=0; feed_i<this.nr_of_feeds; feed_i++) {       
        for (var search_str_i=0; search_str_i<(search_str_arr.length-1); search_str_i++) {
            for (var entry_i=0; entry_i<this.nr_of_entries[feed_i]; entry_i++) {
            
                str = this.rssrx[feed_i][entry_i][this.I.DCR] + this.SPR.ANLS
                    + this.rssrx[feed_i][entry_i][this.I.TTL] + this.SPR.ANLS
                    + this.rssrx[feed_i][entry_i][this.I.HDR].src_pbl;
                
                str_lowercase = str.toLowerCase();
                kMem = k;
    
                for (var ch_i=0, ch_offs=0; ch_i<str_lowercase.length; ch_i++, ch_offs=0) {
                    // jump over <....>
                    if (str.charAt(ch_i) == '<') {
                        while (++ch_i < str.length) {
                            if (str.charAt(ch_i) == '>') {
                                break;
                            }
                        }
                        
                    // detect if equality first letter
                    } else if (str_lowercase.charAt(ch_i) == search_str_arr[search_str_i].charAt(0)) {
                        detect = true;
                        // detect if equality of the following letters
                        while (ch_offs<search_str_arr[search_str_i].length) {
                            if (str_lowercase.charAt(ch_i + ch_offs) != search_str_arr[search_str_i].charAt(ch_offs++)) {
                                detect = false;
                                break;
                            }
                        }
                        
                        ch_i += ch_offs - 1;
                        
                        // if equality detected for search word at search_str_arr[search_str_i]
                        if (detect) {
                            cd[k] = str;
                            cl[k] = this.rssrx[feed_i][entry_i][this.I.URL];
                            
                            if (kMem == k) {
                                k++;
                            }
                        }
                    }
                }
            }
        }
    }
    
    // count duplicates
    for (var i=0; i<k; i++) {
        detect = 0;
    
        for (j=0; j<cl.length; j++)
            if (cl[j] == cl[i])
                detect++;
        
        duplicates[i] = detect;
    }
    
    // delete duplicate entries
    for (var i=0; i<cl.length; i++) {
        for (var j=0; j<cl.length; j++) {
            if (j!=i && cl[i]==cl[j]) {
                cl.splice(i, 1);
                cd.splice(i, 1);
                duplicates.splice(i, 1);
            }
        }
    }
    
    // sort from high to low
    do {
        detect = false;
        for (var i=0; i<=duplicates.length; i++) {
            if (duplicates[i] < duplicates[i+1]) {
                detect = true;
                // init duplicates counter list
                temp = duplicates[i];
                duplicates[i] = duplicates[i+1];
                duplicates[i+1] = temp;
                // init link list
                temp = cl[i];
                cl[i] = cl[i+1];
                cl[i+1] = temp;
                // init description list
                temp = cd[i];
                cd[i] = cd[i+1];
                cd[i+1] = temp;
            }
        }
    } while (detect);
    
    // construct and display the search results
    
    if (cl.length > 0) {
        var patt;
        var r = new Array();
    
        for (i=0; i<cl.length; i++) {
            // r[0] = description, r[1] = title, r[2] = source | publication_date
            r = cd[i].split(this.SPR.ANLS);
            
            // highlight
            for (var j=0; j<(search_str_arr.length-1); j++) {
                patt = new RegExp(search_str_arr[j], 'gi');
                str = 
                    '<font style="background-color:#F0CA38">'
                        +search_str_arr[j]
                    +'</font>';
                
                r[0] = r[0].replace(patt, str);
                r[1] = r[1].replace(patt, str);
            }
            
            feedback += 
                '<a target="_blank" href="'+ cl[i] +'">'+ r[1] +'</a><br/>'
                    +r[2]+'<br/>'+r[0]+'<br/><br/>';
        }
    } else {
        feedback += "<br/>nothing found";
    }
    
    $(this.JQID.SEARCH_RES).html(feedback);
    $(this.JQID.SEARCH_RES + ',' + this.JQID.SEARCH_TAB).css("display", "inline");
    $(this.JQID.SEARCH_TAB)[0].click();
};

// THATS ALL FOLKS :)

/*]]>*/ 
</script>